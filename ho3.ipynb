{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "special-alaska",
   "metadata": {},
   "source": [
    "# Comparison of Linear Models for Machine Learning\n",
    "\n",
    "This notebook, when committed and pushed to your git repository, counts as your\n",
    "submission for Hands-On Assignment 3.\n",
    "\n",
    "GitLab will log the time of updates to your remote repository and pushes after\n",
    "the deadline will count towards your use of late days (just as they would with \n",
    "Canvas). \n",
    "\n",
    "After the homework is due, a python script will execute the notebook, running specific \n",
    "cells tagged `q1`, `q2a`, etc., and test any required outputs, variable declarations,\n",
    "or functions for correctness. These metadata tags may inspected clicking `View` >\n",
    "`Cell Toolbar` > `Tags` (if running `jupyter notebook`) or `View` > `Right Toolbar`\n",
    "(if running `jupyter lab`). Any written answers will be manually graded by the TA.\n",
    "\n",
    "Before you push your changes, we encourage you to run `pytest` from the command\n",
    "line  (with the appropriate conda environment active) in your repository. This \n",
    "will perform a series of tests specified by `ho3_test.py` to ensure that your \n",
    "notebook will be correctly run by the grading script.\n",
    "\n",
    "**Note:** Cells executed by the autograding script may contain no import \n",
    "statements! The only libraries you may use are already loaded for you in\n",
    "cell `q0` (the first code cell in this notebook). This cell will be reset\n",
    "before the grading script runs (no changes you make to this cell will be \n",
    "retained).\n",
    "___\n",
    "## Assignment Structure\n",
    "\n",
    "In part 1, we'll visualize some of the differences between various linear classifiers.\n",
    "\n",
    "In part 2, we'll compare the performance of different linear classifiers on the dataset prepared during Hands-On Assignment 2 (life expectancy vs. economic and social indicators)\n",
    "\n",
    "In part 3, you are encouraged to explore parameter space to find a classifier with high accuracy on this dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "existing-involvement",
   "metadata": {
    "tags": [
     "q0"
    ]
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import sklearn.linear_model\n",
    "from sklearn.impute import KNNImputer\n",
    "from scipy import stats\n",
    "\n",
    "# allow interactive plots embedded in notebook\n",
    "%matplotlib widget\n",
    "\n",
    "# Load data from Hands On Assignment 2\n",
    "df = pd.read_csv('df_final.csv', index_col=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "specified-crowd",
   "metadata": {},
   "source": [
    "___\n",
    "# Part 1: Visualization of Linear Classifier Boundaries\n",
    "\n",
    "A linear classifier maps a linear combination of input features $(x_1, x_2, ..., x_n)$ plus a bias term $b$ to a real-valued signal $s$ with a (typically *non-linear*) function $f$.\n",
    "$$\n",
    "s(\\mathbf{x}) = f\\big(b + w_1 x_1 + w_2 x_2 + ... + w_n x_n \\big) \\\\\n",
    "$$\n",
    "To be concise, we may set $x_0 = 1$ and re-label the variable $b$ as $w_0$ to write\n",
    "$$\n",
    "s(\\mathbf{x}) = f(\\mathbf{w}^\\top \\mathbf{x})\n",
    "$$\n",
    "where $\\mathbf{w}^\\top \\mathbf{x}$ is merely the dot product $\\mathbf{x} \\cdot \\mathbf{w} = \\sum_{i=0}^n w_i x_i = b + \\sum_{i=1}^n w_i x_i$\n",
    "\n",
    "During training, the output of $s(\\mathbf{x})$ of the classifier is compared to the true label $y$ for a large set of known training examples $\\mathbf{x}$, and the weights and bias of the classifier are repeatedly adjusted to minimize a loss function $l(s, y)$ that measures the discrepancy between $s$ and $y$.\n",
    "\n",
    "In machine learning, it's common to encounter some ugly expression like the following which encodes everything we just stated above:\n",
    "$$\n",
    "\\mathbf{w}^* = \\underset{\\mathbf{w}}{\\text{argmin}} \\underset{(\\mathbf{x}, y) \\sim \\mathcal{D}}{\\mathbb{E}} \\Bigg[ L\\bigg(\\mathbf{w}^\\top \\mathbf{x}, y \\bigg) \\Bigg]\n",
    "$$\n",
    "\n",
    "In English: the final weights (and bias) $\\mathbf{w}^*$ of the classifier are those which minimize the *expected* loss $L$ between the output of the classifier $s = \\mathbf{w}^\\top \\mathbf{x}$ and the true label $y$ when $\\mathbf{x}$ and $y$ are sampled together from a distribution $\\mathcal{D}$ that the training set should (empirically) approximate. Because the distribution $\\mathcal{D}$ can only be *empirically* approximated by the training set, machine learning of this sort is sometimes referred to as Empirical Risk Minimization (ERM).\n",
    "\n",
    "We are now equipped to discuss some differences between various linear classifiers, which ultimately come down to differences in the function $f$, the loss function $L$, and the training method by which $\\mathbf{w}$ is updated.\n",
    "\n",
    "To visualize the differences between linear classifiers, we will predict `Life expectancy` from only two features.\n",
    "___"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "healthy-announcement",
   "metadata": {},
   "source": [
    "## Question 1\n",
    "\n",
    "We compare three different loss functions: `perceptron`, `log`, and `hinge`.\n",
    "\n",
    "For each of these models, the decision boundary is given by the line\n",
    "$$\n",
    "\\mathbf{w}^\\top \\mathbf{x} = 0\n",
    "$$\n",
    "Examples for which the left-hand side of this equation is greater than zero will be classified as positive, and those less than zero will be negative. In this case, the role of $f$ manifests in calculating loss rather than determining the output of binary classification.\n",
    "\n",
    "The function `q1a` below, when completed, must return the value of $x_2$ given $w_0$, $w_1$, $w_2$ (as learned by the model) and $x_1$ on the decision boundary. `q2a` should return $x_1$ on the decision boundary given $x_2$, $w_0$, $w_1$, and $w_2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "preliminary-treat",
   "metadata": {
    "tags": [
     "q1"
    ]
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "39c313bd764340b1b213e26fa5eea494",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "def q1a(x1, w_vec):\n",
    "    '''\n",
    "    This function is used to plot the decision boundary of the classifier.\n",
    "    \n",
    "    It should return the value of x2 given x1, and w_vec.\n",
    "    \n",
    "    Args:\n",
    "        x1: the first component of the feature vector\n",
    "        w_vec: the vector of weights (with bias as index 0)\n",
    "        \n",
    "    Returns:\n",
    "        x2: the second component of the feature vector\n",
    "        \n",
    "    '''\n",
    "    return np.nan\n",
    "\n",
    "def q1b(x2, w_vec):\n",
    "    '''\n",
    "    This function is used to plot the decision boundary of the classifier.\n",
    "    \n",
    "    It should return the value of x1 given x2, and w_vec.\n",
    "    \n",
    "    Args:\n",
    "        x2: the second component of the feature vector\n",
    "        w_vec: the vector of weights (with bias as index 0)\n",
    "        \n",
    "    Returns:\n",
    "        x1: the first component of the feature vector\n",
    "        \n",
    "    '''\n",
    "    return np.nan\n",
    "\n",
    "Classifier = sklearn.linear_model.SGDClassifier\n",
    "kwargs = {\n",
    "    'penalty': None, # apply no regularization to constrain learned weights\n",
    "    'average': True # average learned weights and bias across training rounds\n",
    "}\n",
    "models = {\n",
    "    'logistic': Classifier(loss='log',  **kwargs),\n",
    "    'perceptron': Classifier(loss=\"perceptron\",  **kwargs),\n",
    "    'hinge': Classifier(loss='hinge',  **kwargs)\n",
    "}\n",
    "\n",
    "# define features and labels\n",
    "label_column = 'Life expectancy'\n",
    "col0 = 'Internet access'\n",
    "col1 = 'Literacy'\n",
    "\n",
    "feature_columns = [col0, col1]\n",
    "\n",
    "pos = df[df[label_column] > 0]\n",
    "neg = df[df[label_column] < 0]\n",
    "\n",
    "fig = plt.figure(figsize=(8, 6))\n",
    "fig.suptitle('Decision Boundaries of Linear Classifiers with Different Loss Functions')\n",
    "ax1 = fig.add_subplot(111, label=\"q1\")\n",
    "ax1.set_xlabel(col0)\n",
    "ax1.set_ylabel(col1)\n",
    "ax1.scatter(pos[col0], pos[col1], alpha=0.5, c='b', label=f'Above average {label_column}')\n",
    "ax1.scatter(neg[col0], neg[col1], alpha=0.5, c='r', label=f'Below average {label_column}')\n",
    "\n",
    "X = df[feature_columns]\n",
    "Y = df[label_column] > 0 # 0 is average life expectancy\n",
    "\n",
    "# Use K Nearest Neighbors imputation\n",
    "imputer = KNNImputer(n_neighbors=2)\n",
    "\n",
    "# Impute missing data in training set\n",
    "X_imputed = imputer.fit_transform(X)\n",
    "\n",
    "# train classifiers and plot decision boundaries\n",
    "q1_flag = False\n",
    "min_x, max_x = xl, xr = min(df[col0]), max(df[col0])\n",
    "min_y, max_y = min(df[col1]), max(df[col1])\n",
    "for model_name, model in models.items():\n",
    "    model.fit(X_imputed, Y)\n",
    "    \n",
    "    if (q1a(1, np.array([1, 1, 1])) is np.nan) or q1a(1, np.array([1, 1, 1])) is np.nan:\n",
    "        w1, w2 = model.coef_[0]\n",
    "        w0 = model.intercept_[0]\n",
    "        w_vec = np.array([w0, w1, w2])    \n",
    "        yl, yr = q1a(min_x, w_vec), q1a(max_x, w_vec)\n",
    "        if yl < min_y:\n",
    "            yl, xl = min_y, q1b(min_y, w_vec)\n",
    "        if yl > max_y:\n",
    "            yl, xl = max_y, q1b(max_y, w_vec)\n",
    "        if yr < min_y:\n",
    "            yr, xr = min_y, q1b(min_y, w_vec)\n",
    "        if yr > max_y:\n",
    "            yr, xr = max_y, q1b(max_y, w_vec)\n",
    "\n",
    "        ax1.plot(\n",
    "            [xl, xr], # x-coord\n",
    "            [yl, yr], # y-coord\n",
    "            label=model_name\n",
    "        )\n",
    "        \n",
    "    else:\n",
    "        q1_flag = True\n",
    "        \n",
    "if q1_flag:\n",
    "    print('Decision boundaries will display after q1 is edited.')\n",
    "\n",
    "plt.legend()\n",
    "fig.canvas.draw()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "portuguese-dublin",
   "metadata": {},
   "source": [
    "___\n",
    "We note that difference in final decision boundaries is not strongly dependent on loss function when averaging weight values over all updates. When we do not average, boundaries depend strongly on initial conditions and direct comparisons are difficult.\n",
    "___\n",
    "## Question 1c (Written)\n",
    "\n",
    "Explain why the decision boundaries for classifiers with different loss functions may differ."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "through-dimension",
   "metadata": {
    "tags": [
     "q1c"
    ]
   },
   "source": [
    "## <span style=\"color: blue;\">Answer 1c</span>\n",
    "\n",
    "`Your answer here`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "functional-ultimate",
   "metadata": {},
   "source": [
    "___\n",
    "## Question 2\n",
    "\n",
    "Next, we will investigate the role of *regularization*, which is a way of enforcing certain constraints on the parameters $\\mathbf{w}$ of the classifier by penalizing certain properties of $\\mathbf{w}$, such as total magnitude. Typically, regularization is used to enforce something akin to Ockham's Razor by penalizing complicated models in general.\n",
    "\n",
    "$$\n",
    "\\mathbf{w}^* = \\underset{\\mathbf{w}}{\\text{argmin}} \\underset{(\\mathbf{x}, y) \\sim \\mathcal{D}}{\\mathbb{E}} \\Bigg[ L\\bigg(\\mathbf{w}^\\top \\mathbf{x}, y \\bigg) + R(\\mathbf{w}) \\Bigg]\n",
    "$$\n",
    "\n",
    "Modify function `q2a` below so that it computes the $l_1$ norm of a given vector $\\mathbf{w}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "prescription-aggregate",
   "metadata": {
    "tags": [
     "q2a"
    ]
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "27ebd65ff0f14151ac6e29dbacf537a9",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "l1 norm of coefficients for Unconstrained: 1\n",
      "Accuracy of Unconstrained: 0.8097345132743363\n",
      "l1 norm of coefficients for Regularized: 1\n",
      "Accuracy of Regularized: 0.7920353982300885\n",
      "\n",
      "Decision boundaries will display after q1 is edited.\n"
     ]
    }
   ],
   "source": [
    "def q2a(w_vec):\n",
    "    '''\n",
    "    This function calculates the l1 norm of a given\n",
    "    weight vector, which may be used as a penalty to\n",
    "    train the classifier\n",
    "    \n",
    "    Args:\n",
    "        w_vec: a numpy array\n",
    "    \n",
    "    Returns:\n",
    "       the l1 norm of w_vec\n",
    "    \n",
    "    '''\n",
    "    return np.nan\n",
    "\n",
    "Classifier = sklearn.linear_model.SGDClassifier\n",
    "kwargs = {\n",
    "    'loss': 'log',\n",
    "    'average': False, # average the value of the learned weights over updates\n",
    "}\n",
    "models = {\n",
    "    'Unconstrained': Classifier(penalty=None, **kwargs),\n",
    "    'Regularized': Classifier(penalty='l1', alpha=0.1, **kwargs),\n",
    "}\n",
    "\n",
    "# define features and labels\n",
    "label_column = 'Life expectancy'\n",
    "col0 = 'Internet access'\n",
    "col1 = 'Population'\n",
    "\n",
    "feature_columns = [col0, col1]\n",
    "\n",
    "pos = df[df[label_column] > 0]\n",
    "neg = df[df[label_column] < 0]\n",
    "\n",
    "fig = plt.figure(figsize=(8, 6))\n",
    "fig.suptitle('Decision Boundaries of Linear Classifiers with Different Loss Functions')\n",
    "ax1 = fig.add_subplot(111, label=\"q1\")\n",
    "ax1.set_xlabel(col0)\n",
    "ax1.set_ylabel(col1)\n",
    "ax1.scatter(pos[col0], pos[col1], alpha=0.5, c='b', label=f'Above average {label_column}')\n",
    "ax1.scatter(neg[col0], neg[col1], alpha=0.5, c='r', label=f'Below average {label_column}')\n",
    "\n",
    "X = df[feature_columns]\n",
    "Y = df[label_column] > 0 # 0 is average life expectancy\n",
    "\n",
    "# Use K Nearest Neighbors imputation\n",
    "imputer = KNNImputer(n_neighbors=2)\n",
    "\n",
    "# Impute missing data in training set\n",
    "X_imputed = imputer.fit_transform(X)\n",
    "\n",
    "# train classifiers and plot decision boundaries\n",
    "q1_flag = False\n",
    "min_x, max_x = min(df[col0]), max(df[col0])\n",
    "min_y, max_y = min(df[col1]), max(df[col1])\n",
    "for model_name, model in models.items():\n",
    "    model = model.fit(X_imputed, Y)\n",
    "    \n",
    "    if (q1a(1, np.array([1, 1, 1])) is np.nan) or q1a(1, np.array([1, 1, 1])) is np.nan:\n",
    "        w1, w2 = model.coef_[0]\n",
    "        w0 = model.intercept_[0]\n",
    "        w_vec = np.array([w0, w1, w2])\n",
    "        \n",
    "        yl, yr = q1a(min_x, w_vec), q1a(max_x, w_vec)\n",
    "        xl, xr = min_x, max_x\n",
    "        \n",
    "        if yl < min_y:\n",
    "            yl, xl = min_y, q1b(min_y, w_vec)\n",
    "        if yl > max_y:\n",
    "            yl, xl = max_y, q1b(max_y, w_vec)\n",
    "        if yr < min_y:\n",
    "            yr, xr = min_y, q1b(min_y, w_vec)\n",
    "        if yr > max_y:\n",
    "            yr, xr = max_y, q1b(max_y, w_vec)\n",
    "\n",
    "        ax1.plot(\n",
    "            [xl, xr], # x-coord\n",
    "            [yl, yr], # y-coord\n",
    "            label=model_name\n",
    "        )\n",
    "    else:\n",
    "        q1_flag = True\n",
    "        \n",
    "    penalty = q2a(model.coef_)\n",
    "    print(f'l1 norm of coefficients for {model_name}: {penalty}')\n",
    "    print(f'Accuracy of {model_name}:', sum((model.predict(X_imputed) > 0) == (Y > 0)) / len(df))\n",
    "        \n",
    "if q1_flag:\n",
    "    print()\n",
    "    print('Decision boundaries will display after q1 is edited.')\n",
    "\n",
    "plt.legend()\n",
    "fig.canvas.draw()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "adolescent-techno",
   "metadata": {},
   "source": [
    "___\n",
    "## Question 2b (Written)\n",
    "\n",
    "Explain why the decision boundaries for the l1-regularized classifier and the unconstrained classifier may differ."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "metric-upset",
   "metadata": {
    "tags": [
     "q2b"
    ]
   },
   "source": [
    "## <span style=\"color: blue;\">Answer 2b</span>\n",
    "\n",
    "`Your answer here`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "chicken-vaccine",
   "metadata": {
    "tags": []
   },
   "source": [
    "___\n",
    "# Part 2 (Modeling Real-World Data)\n",
    "\n",
    "We now try to visualize the effect of regularization by training models on our real-world data. You don't need to write new code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "senior-puppy",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "37d265acbf14437c93dfeeafff41bf77",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Classifier = sklearn.linear_model.SGDClassifier\n",
    "kwargs = {\n",
    "    'loss': 'perceptron',\n",
    "    'average': False, # average the value of the learned weights over updates\n",
    "    'fit_intercept': True\n",
    "}\n",
    "models = {\n",
    "    'Unconstrained': {'penalty': None},\n",
    "    'L1 Regularized': {'penalty': 'l1', 'alpha': 0.01}\n",
    "}\n",
    "\n",
    "training_results = {\n",
    "    # model_name: (train_acc, test_acc)\n",
    "    n: {\n",
    "        'coef_': [],\n",
    "        'intercept_': [],\n",
    "        'l1': []\n",
    "    } for n in models.keys()\n",
    "}\n",
    "    \n",
    "# load data\n",
    "df = pd.read_csv('df_final.csv', index_col=0)\n",
    "    \n",
    "k = 10\n",
    "\n",
    "# train each model on the full dataset k times\n",
    "for i in range(k):\n",
    "\n",
    "    # define features and labels\n",
    "    label_column = 'Life expectancy'\n",
    "    feature_columns = ['Electricity access', \n",
    "                       'fish', 'machinery', 'chemicals',\n",
    "                       'petroleum', 'oil', 'equipment',\n",
    "                       'coffee', 'textiles', 'cotton',\n",
    "                       'gold', 'metals', 'foodstuffs',\n",
    "                       'crude', 'sugar', 'clothing'\n",
    "    ]\n",
    "\n",
    "    X = df[feature_columns]\n",
    "    Y = df[label_column] > 0 # 0 is the rescaled mean\n",
    "        \n",
    "    # Use K Nearest Neighbors imputation\n",
    "    imputer = KNNImputer(n_neighbors=2)\n",
    "        \n",
    "    # Impute missing data in training set\n",
    "    X_imputed = imputer.fit_transform(X)\n",
    "\n",
    "    for model_name, model_params in models.items():\n",
    "        # train a classifier on the imputed training set\n",
    "        model = Classifier(**model_params, **kwargs)\n",
    "        model = model.fit(X_imputed, Y)\n",
    "\n",
    "        # Save model parameters\n",
    "        training_results[model_name]['coef_'].append(model.coef_)\n",
    "        training_results[model_name]['intercept_'].append(model.intercept_)\n",
    "        training_results[model_name]['l1'].append(np.sum(np.abs(model.coef_)))\n",
    "        \n",
    "# convert values of training_results into numpy arrays\n",
    "for m in training_results:\n",
    "    training_results[m]['coef_'] = np.array(training_results[m]['coef_'])\n",
    "    training_results[m]['intercept_'] = np.array(training_results[m]['intercept_'])\n",
    "\n",
    "prop_cycle = plt.rcParams['axes.prop_cycle']\n",
    "colors = prop_cycle.by_key()['color']\n",
    "\n",
    "fig = plt.figure(figsize=(12, 6))\n",
    "fig.suptitle('Absolute Vaule of Coefficients Learned by Classifiers, Normalized by L1 Norm, Compared to Feature-Label Correlations')\n",
    "\n",
    "ax = fig.add_subplot(111, label=\"q3\")\n",
    "\n",
    "for index, [model_name, v] in enumerate(training_results.items()):\n",
    "    for j in range(k):\n",
    "        kwargs = ({} if j > 0 else {'label': model_name})\n",
    "        ax.plot(v['coef_'][j][0] / v['l1'][j], alpha=0.5, color=colors[index], **kwargs)\n",
    "        \n",
    "cw = df[feature_columns].corrwith(df['Life expectancy'] > 0)\n",
    "ax.bar(feature_columns, cw/np.sum(np.abs(cw)), alpha=0.2, color=colors[2], label='Feature Correlation with Label')\n",
    "fig.autofmt_xdate(rotation=45)\n",
    "ax.set_ylabel('Weight')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "abandoned-pizza",
   "metadata": {},
   "source": [
    "___\n",
    "## Question 3 (Written)\n",
    "\n",
    "Explain how regularization can help generate simpler models."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "offshore-worship",
   "metadata": {
    "tags": [
     "q3"
    ]
   },
   "source": [
    "## <span style=\"color: blue;\">Answer 3</span>\n",
    "\n",
    "`Your answer here`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "angry-cosmetic",
   "metadata": {},
   "source": [
    "___\n",
    "\n",
    "We now compare loss regularization penalties in terms of accuracy when applied to our full dataset. There's no new code you need to write for this comparison. Note that the regularized loss is much higher for this example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "municipal-depth",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "8c5e3e7a6abf4904b3dbb3245a319c16",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "Classifier = sklearn.linear_model.SGDClassifier\n",
    "kwargs = {\n",
    "    'loss': 'perceptron',\n",
    "    'average': False, # average the value of the learned weights over updates\n",
    "}\n",
    "models = {\n",
    "    'L1': Classifier(penalty='l1', alpha=0.1, **kwargs),\n",
    "    'Unconstrained': Classifier(penalty=None, **kwargs),\n",
    "}\n",
    "\n",
    "k = 25\n",
    "def k_fold_validate(df, models, k=k):\n",
    "    \n",
    "    results_dict = {\n",
    "        # model_name: (train_acc, test_acc)\n",
    "        n: {\n",
    "            'train_acc': [],\n",
    "            'test_acc': []\n",
    "        } for n in models.keys()\n",
    "    }\n",
    "    \n",
    "    for i in range(k):\n",
    "        # split into training set and testing set at random\n",
    "        df_train = df.sample(frac=0.5)\n",
    "        df_test = df.drop(df_train.index)\n",
    "\n",
    "        # define features and labels\n",
    "        label_column = 'Life expectancy'\n",
    "        feature_columns = [c for c in df.columns if c != label_column]\n",
    "\n",
    "        X_train = df_train[feature_columns]\n",
    "        Y_train = df_train[label_column] > 0 # 0 is the rescaled mean\n",
    "        X_test = df_test[feature_columns]\n",
    "        Y_test = df_test[label_column] > 0 # 0 is the rescaled mean\n",
    "        \n",
    "        # Use K Nearest Neighbors imputation\n",
    "        imputer = KNNImputer(n_neighbors=2)\n",
    "        \n",
    "        # Impute missing data in training set\n",
    "        X_train_imputed = imputer.fit_transform(X_train)\n",
    "\n",
    "        for model_name, model in models.items():\n",
    "            # train a classifier on the imputed training set\n",
    "            model.fit(X_train_imputed, Y_train)\n",
    "\n",
    "            train_acc = sum(model.predict(X_train_imputed) == Y_train) / len(df_train)\n",
    "            test_acc = sum(model.predict(imputer.transform(X_test)) == Y_test) / len(df_test)\n",
    "            \n",
    "            # predict on the training set\n",
    "            results_dict[model_name]['train_acc'].append(train_acc)\n",
    "        \n",
    "            # predict on the test set after imputing based on the training set\n",
    "            results_dict[model_name]['test_acc'].append(test_acc)\n",
    "        \n",
    "    # convert to numpy array\n",
    "    for m in results_dict:\n",
    "        results_dict[m]['train_acc'] = np.array(results_dict[m]['train_acc'])\n",
    "        results_dict[m]['test_acc'] = np.array(results_dict[m]['test_acc'])\n",
    "    \n",
    "    return results_dict\n",
    "\n",
    "# load data\n",
    "df = pd.read_csv('df_final.csv', index_col=0)\n",
    "\n",
    "fig = plt.figure(figsize=(8, 6))\n",
    "ax1 = fig.add_subplot(111)\n",
    "ax1.set_xlabel('Training Accuracy')\n",
    "ax1.set_ylabel('Test Accuracy')\n",
    "\n",
    "results_dict = k_fold_validate(df, models)\n",
    "\n",
    "def plot_strategy(model_name, model, **kwargs):\n",
    "\n",
    "    train_acc = results_dict[model_name]['train_acc']\n",
    "    test_acc = results_dict[model_name]['test_acc']\n",
    "    \n",
    "    ax1.scatter(train_acc, test_acc, s=4, alpha=0.5, **kwargs)\n",
    "    del kwargs['label']\n",
    "    ax1.scatter(np.mean(train_acc), np.mean(test_acc), marker='+', s=200, alpha=0.5, **kwargs)\n",
    "\n",
    "prop_cycle = plt.rcParams['axes.prop_cycle']\n",
    "colors = prop_cycle.by_key()['color']\n",
    "\n",
    "for index, [model_name, model] in enumerate(models.items()):\n",
    "    plot_strategy(model_name, model, c=colors[index], label=model_name)\n",
    "    \n",
    "ax1.plot([0.5, 0.9], [0.5, 0.9], label=\"perfect generalization to unseen data\", c='k')\n",
    "\n",
    "plt.legend()\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "blond-attendance",
   "metadata": {},
   "source": [
    "## Question 4\n",
    "\n",
    "The above plot depicts a specific tradeoff of regularization. What is it?\n",
    "\n",
    "*hint:* points below the black line are indicative of overfitting."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "peripheral-deficit",
   "metadata": {
    "tags": [
     "q4"
    ]
   },
   "source": [
    "## <span style=\"color: blue;\">Answer 4</span>\n",
    "\n",
    "`Your answer here`"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
