# Hands-On Assigment 3

This assignment involves comparing linear classifiers on real-world data.

* Complete the assignment by running `jupyter lab` (or `jupyter notebook`) and editing ho3.ipynb.
* Run `pytest` in this directory to test your edits.
* Add, Commit, and Push your edits to your individual repository to submit the assignment.
