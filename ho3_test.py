#!/usr/bin/env python3

import pytest
import testbook as tb
import pandas as pd
import json

exe_cells = [
    'q0', 'q1', 'q2a'
]

@pytest.fixture(scope='module')
def nb():
    with tb.testbook('ho3.ipynb', execute=exe_cells) as nb:
        yield nb

@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q1a(nb):

    print('The Function `q1a` must return a float.')

    nb.inject('assert isinstance(q1a(1, np.array([1, 1, 1])), np.float)')

@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q1b(nb):

    print('The Function `q1b` must return a float.')

    nb.inject('assert isinstance(q1b(1, np.array([1, 1, 1])), np.float)')

@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q2a(nb):

    print('The Function `q2a` must return a float.')

    nb.inject('assert isinstance(q2a(np.array([1, 1, 1])), np.float)')
